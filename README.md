# Easy HTML Generator
Forget the old way of generating HTML markups for Email. This boilerplate gives you a new dimension for creating static web apps using React, ES6, SASS & Webpack with linting based production build.

Use this boilerplate if you want to,
## Create email supported HTML markup
The generated HTML markup for email will be compatible with all/most of the email clients. The below points have been taken care to achieve the compatibility.
# 
* XHTML 1.0 Strict DOCTYPE
* Standards compliant. This boilerplate code passed the W3C HTML markup validation
* `<script></script>` tags free
* Styles have been embedded in the head section
* No forms or iframe added

## Create simple/complex HTML markup
This is the normal mode to generate single HTML. You can add all kinds of JavaScript behaviours like Forms, Routing (Static/Dynamic). The only requirement is that it will generate a single HTML. All the styles and scripts will be embedded into the HTML page in dist folder. If your HTML page has too many functionalities or styles, then it might bump up the HTML size. Alternatively, you can make use of the separated and minified CSS/JS files in the build folder.

# Features
1. Interactive production build
2. Component based markup using React
3. Behavior-based HTML/CSS markup
4. JavaScript Linting
5. SASS Linting
6. ES6
7. Generates single HTML markup and minified HTML markup with separated JS and CSS

# Usage
1. npm install -g inline-source-cli
2. npm install -g webpack-dev-server
3. Clone this repo
4. Hop into the root of the cloned folder
5. npm install

## Development
1. npm start
2. We create sample Header and Footer components to start with
3. Write all your styles in SASS in 'css' folder

## Production
1. sh ./bin/start.sh
2. Choose if you want to generate Email or Landing page
3. After successful production build, 
- 'dist' folder will have HTML file with embedded JS and CSS
- 'build' folder will have HTML file CSS and JS files

# Happy coding!!!