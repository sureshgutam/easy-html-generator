read -p "$(echo '\n|-|\n|-|\n|-|\n|-|\n|-|\n
What type of HTML do you want to generate? \n
1 - Landing
2 - Email\n
Please choose an option (1 or 2): ')" option
if [ $option == 1 ]
then
    echo "\nYou have opted to generate Landing HTML\n"
    echo "\n|-|\n|-|\n|-|\n|-|\n|-|\n"
    echo "Generating Landing page HTML....\n"
    sh ./bin/generateLandingPage.sh
else
    echo "\nYou have opted to generate Email HTML\n"
    echo "\n|-|\n|-|\n|-|\n|-|\n|-|\n"
    echo "Generating Email page HTML....\n"
    sh ./bin/generateEmailPage.sh
fi
