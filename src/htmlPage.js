export default ({
  app = '', main = '', title = 'Easy Email Markup Generator'
}) => `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>${title}</title>
      <link rel="stylesheet" type="text/css" inline href="./main.css">
    </head>
    <body>
      <div id="app">${app}</div>
      <script type="text/javascript" inline src=${main}></script>
    </body>
  </html>
`;
