import React from 'react';
import ReactDOM from 'react-dom';
import { renderToStaticMarkup } from 'react-dom/server';
import App from './App';
import htmlPage from './htmlPage';
import './css/App.scss';

if (typeof document !== 'undefined') {
  ReactDOM.render(
    <App />,
    document.getElementById('app')
  );
}

const Entry = (locals) => {
  if (!locals) return true;
  return htmlPage({
    app: renderToStaticMarkup(<App />),
    main: locals.assets.main
  });
};

export default Entry;
