import React from 'react';
import Header from './components/Header';
import Footer from './components/Footer';

const App = () => (
  <div>
    <Header />
    <div className="row">
      <h2 className="col-lg-12">Easy HTML Generator</h2>
      <p className="col-lg-12">
        This is a boilerplate for generating static HTML using react and webpack. <br />
        Forget the old school way of generating email markup.
        Create email markups in a component based architecture.<br />
        This follows the XHTML strict DOCTYPE which gives support for email compatibility.
      </p>
      <div className="col-lg-3 col-md-6 col-sm-12 sections">Section-1</div>
      <div className="col-lg-3 col-md-6 col-sm-12 sections">Section-2</div>
      <div className="col-lg-3 col-md-6 col-sm-12 sections">Section-3</div>
      <div className="col-lg-3 col-md-6 col-sm-12 sections">Section-4</div>
    </div>
    <Footer />
  </div>
);

export default App;
