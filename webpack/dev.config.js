const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const createConfig = require('./createConfig');

module.exports = createConfig({
  outputFilename: '[name].js',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }),
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 3000,
      server: { baseDir: ['build'] }
    })
  ]
});
