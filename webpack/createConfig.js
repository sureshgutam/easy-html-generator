const path = require('path');
const StaticSiteGeneratorWebpackPlugin = require('static-site-generator-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const commonConfig = function commonConfig(options) {
  return {
    entry: {
      main: './src/entry.js'
    },
    output: {
      filename: options.outputFilename,
      path: path.resolve('build'),
      libraryTarget: 'umd'
    },
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'eslint-loader'
        },
        {
          test: /.js$/,
          include: [
            /src/
          ],
          exclude: [path.resolve(__dirname, 'src/css'), /node_modules/],
          use: [
            'babel-loader'
          ]
        },
        {
          test: /\.s?css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              'css-loader', 'sass-loader'
            ],
            publicPath: '/build/'
          })
        }
      ]
    },
    plugins: [
      new ExtractTextPlugin({
        filename: '[name].css',
        disable: false,
        allChunks: true
      }),
      new StaticSiteGeneratorWebpackPlugin('main', ['/'])
    ].concat(options.plugins)
  };
};

module.exports = commonConfig;
